#!/bin/bash


CASKS=(
  dbeaver-community
  insomnia
  rectangle
  evernote
  slack
  alacritty
)

echo "Installing Dev Tools"
xcode-select --install

# Install applications from App Store
echo "Installing Xcode"
mas install 497799835 # Xcode

echo "Checking if homebrew is installed"
which brew
if [[ $? -ne 0 ]]
then
  echo "Installing homebrew..."
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  (echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> /Users/$(whoami)/.zprofile
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

echo "Setting up .zshrc if necessary"
ls ~/.zshrc
if [[ $? -ne 0 ]]
then
  touch ~/.zshrc
fi

echo "Installing brew packages"
brew tap homebrew/cask-versions
brew update
brew install vim --override-system-vi
brew install --cask --appdir="/Applications" ${CASKS[@]}
brew tap homebrew/cask-fonts
brew install --cask font-hack-nerd-font
brew cleanup
