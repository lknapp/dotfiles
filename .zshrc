python3 ~/projects/astrology/info.py

PHASE=$(python3 ~/projects/astrology/phase.py)
SIGN=$(python3 ~/projects/astrology/sign.py)
PROMPT=" ${SIGN} ${PHASE} %(?.%F{green}+.%F{red}?%?)%f %B%F{240}%1~%f%b %# "

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

export ANDROID_HOME=/Users/louisknapp/Library/Android/sdk
export ANDROID_NDK_HOME=/Users/louisknapp/Library/Android/sdk/ndk
